import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {NotFoundComponent} from "./not-found/not-found.component";

const routes: Routes = [
  {path: 'elements',loadChildren: ()=> import('./elements/elements.module').then(e=>e.ElementsModule)},
  {path: 'collections',loadChildren: ()=> import('./collections/collections.module').then(c=>c.CollectionsModule)},
  {path: 'views',loadChildren: ()=> import('./views/views.module').then(v=>v.ViewsModule)},
  {path: 'mods',loadChildren: ()=> import('./mods/mods.module').then(m=>m.ModsModule)},
  {path: '' ,redirectTo:'/Home',pathMatch:'full'},
  {path: 'Home' ,component:HomeComponent},
  {path: 'NotFound' ,component:NotFoundComponent},
  {path: '**' ,redirectTo:'/NotFound'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
